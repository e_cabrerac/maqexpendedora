-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Tue Dec 15 12:40:08 2020
-- Host        : Elena running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/elena/Documents/A404/SED/Trabajos/VHDL/MaquinaExpendedora/MaquinaExpendedora.sim/sim_1/synth/func/xsim/top_tb_func_synth.vhd
-- Design      : top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SYNCHRNZR is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    CLK_P_BUFG : in STD_LOGIC;
    COINS_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end SYNCHRNZR;

architecture STRUCTURE of SYNCHRNZR is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of SYNC_OUT_reg_srl2 : label is "\synchronizers ";
  attribute srl_name : string;
  attribute srl_name of SYNC_OUT_reg_srl2 : label is "\synchronizers[0].inst_synchronizer_i/SYNC_OUT_reg_srl2 ";
begin
SYNC_OUT_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => CLK_P_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => COINS_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SYNCHRNZR_1 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    CLK_P_BUFG : in STD_LOGIC;
    COINS_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of SYNCHRNZR_1 : entity is "SYNCHRNZR";
end SYNCHRNZR_1;

architecture STRUCTURE of SYNCHRNZR_1 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of SYNC_OUT_reg_srl2 : label is "\synchronizers ";
  attribute srl_name : string;
  attribute srl_name of SYNC_OUT_reg_srl2 : label is "\synchronizers[1].inst_synchronizer_i/SYNC_OUT_reg_srl2 ";
begin
SYNC_OUT_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => CLK_P_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => COINS_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SYNCHRNZR_3 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    CLK_P_BUFG : in STD_LOGIC;
    COINS_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of SYNCHRNZR_3 : entity is "SYNCHRNZR";
end SYNCHRNZR_3;

architecture STRUCTURE of SYNCHRNZR_3 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of SYNC_OUT_reg_srl2 : label is "\synchronizers ";
  attribute srl_name : string;
  attribute srl_name of SYNC_OUT_reg_srl2 : label is "\synchronizers[2].inst_synchronizer_i/SYNC_OUT_reg_srl2 ";
begin
SYNC_OUT_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => CLK_P_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => COINS_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SYNCHRNZR_5 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    CLK_P_BUFG : in STD_LOGIC;
    COINS_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of SYNCHRNZR_5 : entity is "SYNCHRNZR";
end SYNCHRNZR_5;

architecture STRUCTURE of SYNCHRNZR_5 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of SYNC_OUT_reg_srl2 : label is "\synchronizers ";
  attribute srl_name : string;
  attribute srl_name of SYNC_OUT_reg_srl2 : label is "\synchronizers[3].inst_synchronizer_i/SYNC_OUT_reg_srl2 ";
begin
SYNC_OUT_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => CLK_P_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => COINS_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity count is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \accum_i_reg[7]_0\ : out STD_LOGIC;
    \accum_i_reg[6]_0\ : out STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_P_BUFG : in STD_LOGIC;
    \accum_i_reg[3]_0\ : in STD_LOGIC;
    Q1 : in STD_LOGIC;
    Q2 : in STD_LOGIC;
    Q3 : in STD_LOGIC;
    \accum_i_reg[3]_1\ : in STD_LOGIC;
    Q1_0 : in STD_LOGIC;
    Q2_1 : in STD_LOGIC;
    Q3_2 : in STD_LOGIC;
    \accum_i_reg[7]_1\ : in STD_LOGIC;
    Q3_3 : in STD_LOGIC;
    Q2_4 : in STD_LOGIC;
    Q1_5 : in STD_LOGIC
  );
end count;

architecture STRUCTURE of count is
  signal accum_i : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \accum_i[7]_i_1_n_0\ : STD_LOGIC;
  signal \accum_i[7]_i_3_n_0\ : STD_LOGIC;
  signal accum_i_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \^out\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \plusOp_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \plusOp_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \plusOp_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \plusOp_carry__0_n_1\ : STD_LOGIC;
  signal \plusOp_carry__0_n_2\ : STD_LOGIC;
  signal \plusOp_carry__0_n_3\ : STD_LOGIC;
  signal plusOp_carry_i_1_n_0 : STD_LOGIC;
  signal plusOp_carry_i_2_n_0 : STD_LOGIC;
  signal plusOp_carry_n_0 : STD_LOGIC;
  signal plusOp_carry_n_1 : STD_LOGIC;
  signal plusOp_carry_n_2 : STD_LOGIC;
  signal plusOp_carry_n_3 : STD_LOGIC;
  signal \NLW_plusOp_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of plusOp_carry : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__0\ : label is 35;
begin
  \out\(0) <= \^out\(0);
\FSM_sequential_current_state[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7F7F7F7F7F7F7FF"
    )
        port map (
      I0 => accum_i_reg(6),
      I1 => accum_i_reg(5),
      I2 => accum_i_reg(7),
      I3 => accum_i_reg(4),
      I4 => accum_i_reg(3),
      I5 => accum_i_reg(2),
      O => \accum_i_reg[6]_0\
    );
\FSM_sequential_current_state[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFFFF"
    )
        port map (
      I0 => accum_i_reg(7),
      I1 => accum_i_reg(4),
      I2 => accum_i_reg(3),
      I3 => \^out\(0),
      I4 => \accum_i[7]_i_3_n_0\,
      O => \accum_i_reg[7]_0\
    );
\accum_i[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAAE"
    )
        port map (
      I0 => AR(0),
      I1 => \accum_i[7]_i_3_n_0\,
      I2 => \^out\(0),
      I3 => accum_i_reg(3),
      I4 => accum_i_reg(4),
      I5 => accum_i_reg(7),
      O => \accum_i[7]_i_1_n_0\
    );
\accum_i[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => accum_i_reg(0),
      I1 => accum_i_reg(6),
      I2 => accum_i_reg(2),
      I3 => accum_i_reg(5),
      O => \accum_i[7]_i_3_n_0\
    );
\accum_i_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(0),
      Q => accum_i_reg(0),
      R => \accum_i[7]_i_1_n_0\
    );
\accum_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(1),
      Q => \^out\(0),
      R => \accum_i[7]_i_1_n_0\
    );
\accum_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(2),
      Q => accum_i_reg(2),
      R => \accum_i[7]_i_1_n_0\
    );
\accum_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(3),
      Q => accum_i_reg(3),
      R => \accum_i[7]_i_1_n_0\
    );
\accum_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(4),
      Q => accum_i_reg(4),
      R => \accum_i[7]_i_1_n_0\
    );
\accum_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(5),
      Q => accum_i_reg(5),
      R => \accum_i[7]_i_1_n_0\
    );
\accum_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(6),
      Q => accum_i_reg(6),
      R => \accum_i[7]_i_1_n_0\
    );
\accum_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => E(0),
      D => accum_i(7),
      Q => accum_i_reg(7),
      R => \accum_i[7]_i_1_n_0\
    );
plusOp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => plusOp_carry_n_0,
      CO(2) => plusOp_carry_n_1,
      CO(1) => plusOp_carry_n_2,
      CO(0) => plusOp_carry_n_3,
      CYINIT => '0',
      DI(3 downto 2) => accum_i_reg(3 downto 2),
      DI(1) => \^out\(0),
      DI(0) => '0',
      O(3 downto 0) => accum_i(3 downto 0),
      S(3) => plusOp_carry_i_1_n_0,
      S(2) => plusOp_carry_i_2_n_0,
      S(1) => S(0),
      S(0) => accum_i_reg(0)
    );
\plusOp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => plusOp_carry_n_0,
      CO(3) => \NLW_plusOp_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \plusOp_carry__0_n_1\,
      CO(1) => \plusOp_carry__0_n_2\,
      CO(0) => \plusOp_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => accum_i_reg(6 downto 4),
      O(3 downto 0) => accum_i(7 downto 4),
      S(3) => accum_i_reg(7),
      S(2) => \plusOp_carry__0_i_1_n_0\,
      S(1) => \plusOp_carry__0_i_2_n_0\,
      S(0) => \plusOp_carry__0_i_3_n_0\
    );
\plusOp_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A999999"
    )
        port map (
      I0 => accum_i_reg(6),
      I1 => \accum_i_reg[7]_1\,
      I2 => Q3_2,
      I3 => Q2_1,
      I4 => Q1_0,
      O => \plusOp_carry__0_i_1_n_0\
    );
\plusOp_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A999999"
    )
        port map (
      I0 => accum_i_reg(5),
      I1 => \accum_i_reg[3]_1\,
      I2 => Q3_3,
      I3 => Q2_4,
      I4 => Q1_5,
      O => \plusOp_carry__0_i_2_n_0\
    );
\plusOp_carry__0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6555AAAA"
    )
        port map (
      I0 => accum_i_reg(4),
      I1 => Q3_2,
      I2 => Q2_1,
      I3 => Q1_0,
      I4 => \accum_i_reg[7]_1\,
      O => \plusOp_carry__0_i_3_n_0\
    );
plusOp_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA6A"
    )
        port map (
      I0 => accum_i_reg(3),
      I1 => Q1_0,
      I2 => Q2_1,
      I3 => Q3_2,
      O => plusOp_carry_i_1_n_0
    );
plusOp_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA55559555"
    )
        port map (
      I0 => accum_i_reg(2),
      I1 => \accum_i_reg[3]_0\,
      I2 => Q1,
      I3 => Q2,
      I4 => Q3,
      I5 => \accum_i_reg[3]_1\,
      O => plusOp_carry_i_2_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer is
  port (
    Q2 : out STD_LOGIC;
    Q1 : out STD_LOGIC;
    Q3 : out STD_LOGIC;
    Q3_reg_0 : out STD_LOGIC;
    CLK_P_BUFG : in STD_LOGIC;
    Q1_reg_0 : in STD_LOGIC
  );
end debouncer;

architecture STRUCTURE of debouncer is
  signal \^q1\ : STD_LOGIC;
  signal \^q2\ : STD_LOGIC;
  signal \^q3\ : STD_LOGIC;
begin
  Q1 <= \^q1\;
  Q2 <= \^q2\;
  Q3 <= \^q3\;
Q1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => Q1_reg_0,
      Q => \^q1\,
      R => '0'
    );
Q2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => \^q1\,
      Q => \^q2\,
      R => '0'
    );
Q3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => \^q2\,
      Q => \^q3\,
      R => '0'
    );
\accum_i[7]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^q3\,
      I1 => \^q2\,
      I2 => \^q1\,
      O => Q3_reg_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer_0 is
  port (
    Q2 : out STD_LOGIC;
    Q1 : out STD_LOGIC;
    Q3 : out STD_LOGIC;
    Q1_reg_0 : out STD_LOGIC;
    Q3_reg_0 : out STD_LOGIC;
    CLK_P_BUFG : in STD_LOGIC;
    Q1_reg_1 : in STD_LOGIC;
    Q1_0 : in STD_LOGIC;
    Q2_1 : in STD_LOGIC;
    Q3_2 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of debouncer_0 : entity is "debouncer";
end debouncer_0;

architecture STRUCTURE of debouncer_0 is
  signal \^q1\ : STD_LOGIC;
  signal \^q2\ : STD_LOGIC;
  signal \^q3\ : STD_LOGIC;
begin
  Q1 <= \^q1\;
  Q2 <= \^q2\;
  Q3 <= \^q3\;
Q1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => Q1_reg_1,
      Q => \^q1\,
      R => '0'
    );
Q2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => \^q1\,
      Q => \^q2\,
      R => '0'
    );
Q3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => \^q2\,
      Q => \^q3\,
      R => '0'
    );
\accum_i[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"08080808FF080808"
    )
        port map (
      I0 => \^q1\,
      I1 => \^q2\,
      I2 => \^q3\,
      I3 => Q1_0,
      I4 => Q2_1,
      I5 => Q3_2,
      O => Q1_reg_0
    );
plusOp_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \^q3\,
      I1 => \^q2\,
      I2 => \^q1\,
      O => Q3_reg_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer_2 is
  port (
    Q2 : out STD_LOGIC;
    Q1 : out STD_LOGIC;
    Q3 : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_P_BUFG : in STD_LOGIC;
    Q1_reg_0 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \accum_i_reg[3]\ : in STD_LOGIC;
    \accum_i_reg[3]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of debouncer_2 : entity is "debouncer";
end debouncer_2;

architecture STRUCTURE of debouncer_2 is
  signal \^q1\ : STD_LOGIC;
  signal \^q2\ : STD_LOGIC;
  signal \^q3\ : STD_LOGIC;
begin
  Q1 <= \^q1\;
  Q2 <= \^q2\;
  Q3 <= \^q3\;
Q1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => Q1_reg_0,
      Q => \^q1\,
      R => '0'
    );
Q2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => \^q1\,
      Q => \^q2\,
      R => '0'
    );
Q3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => \^q2\,
      Q => \^q3\,
      R => '0'
    );
plusOp_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555AAAA6AAA"
    )
        port map (
      I0 => \out\(0),
      I1 => \accum_i_reg[3]\,
      I2 => \^q1\,
      I3 => \^q2\,
      I4 => \^q3\,
      I5 => \accum_i_reg[3]_0\,
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer_4 is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_P_BUFG : in STD_LOGIC;
    Q1_reg_0 : in STD_LOGIC;
    \accum_i_reg[7]\ : in STD_LOGIC;
    \accum_i_reg[7]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of debouncer_4 : entity is "debouncer";
end debouncer_4;

architecture STRUCTURE of debouncer_4 is
  signal Q1 : STD_LOGIC;
  signal Q2 : STD_LOGIC;
  signal Q3 : STD_LOGIC;
begin
Q1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => Q1_reg_0,
      Q => Q1,
      R => '0'
    );
Q2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => Q1,
      Q => Q2,
      R => '0'
    );
Q3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      D => Q2,
      Q => Q3,
      R => '0'
    );
\accum_i[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFF08"
    )
        port map (
      I0 => Q1,
      I1 => Q2,
      I2 => Q3,
      I3 => \accum_i_reg[7]\,
      I4 => \accum_i_reg[7]_0\,
      O => E(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    PRODUCT_OBUF : out STD_LOGIC;
    LIGHT_OBUF : out STD_LOGIC_VECTOR ( 4 downto 0 );
    ERROR_OBUF : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[23]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[27]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[30]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ticks_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \FSM_sequential_current_state_reg[0]_1\ : in STD_LOGIC;
    ZERO : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_0\ : in STD_LOGIC;
    CLK_P_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end fsm;

architecture STRUCTURE of fsm is
  signal \FSM_sequential_current_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[2]_i_1_n_0\ : STD_LOGIC;
  signal current_state : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \ticks[0]_i_10_n_0\ : STD_LOGIC;
  signal \ticks[0]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[0]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[0]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[0]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[0]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[0]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[0]_i_9_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_2_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[12]_i_9_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_2_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[16]_i_9_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_2_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[20]_i_9_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_2_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[24]_i_9_n_0\ : STD_LOGIC;
  signal \ticks[28]_i_2_n_0\ : STD_LOGIC;
  signal \ticks[28]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[28]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[28]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[28]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[28]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[28]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_2_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[4]_i_9_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_2_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_3_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_4_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_5_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_6_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_7_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_8_n_0\ : STD_LOGIC;
  signal \ticks[8]_i_9_n_0\ : STD_LOGIC;
  signal \ticks_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \ticks_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \ticks_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \ticks_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \ticks_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \ticks_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \ticks_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \ticks_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \ticks_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \ticks_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \ticks_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \ticks_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \ticks_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \ticks_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \ticks_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \ticks_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \ticks_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \ticks_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \ticks_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \ticks_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \ticks_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \ticks_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \ticks_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \ticks_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \ticks_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \ticks_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \ticks_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \ticks_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \ticks_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \ticks_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \ticks_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \NLW_ticks_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of ERROR_OBUF_inst_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[2]_i_1\ : label is "soft_lutpair1";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s0:000,s3:010,s4:100,s2:011,s1:001";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s0:000,s3:010,s4:100,s2:011,s1:001";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[2]\ : label is "s0:000,s3:010,s4:100,s2:011,s1:001";
  attribute SOFT_HLUTNM of \LIGHT_OBUF[0]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \LIGHT_OBUF[1]_inst_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \LIGHT_OBUF[2]_inst_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \LIGHT_OBUF[3]_inst_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \LIGHT_OBUF[4]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of PRODUCT_OBUF_inst_i_1 : label is "soft_lutpair0";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \ticks_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \ticks_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \ticks_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \ticks_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \ticks_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \ticks_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \ticks_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \ticks_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of zero_i_6 : label is "soft_lutpair4";
begin
ERROR_OBUF_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"24"
    )
        port map (
      I0 => current_state(0),
      I1 => current_state(2),
      I2 => current_state(1),
      O => ERROR_OBUF
    );
\FSM_sequential_current_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CC01"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[0]_1\,
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => current_state(2),
      O => \FSM_sequential_current_state[0]_i_1_n_0\
    );
\FSM_sequential_current_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF0003F5FF0003F0"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[0]_1\,
      I1 => ZERO,
      I2 => current_state(0),
      I3 => current_state(1),
      I4 => current_state(2),
      I5 => \FSM_sequential_current_state_reg[1]_0\,
      O => \FSM_sequential_current_state[1]_i_1_n_0\
    );
\FSM_sequential_current_state[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDC0"
    )
        port map (
      I0 => ZERO,
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => current_state(2),
      O => \FSM_sequential_current_state[2]_i_1_n_0\
    );
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      CLR => AR(0),
      D => \FSM_sequential_current_state[0]_i_1_n_0\,
      Q => current_state(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      CLR => AR(0),
      D => \FSM_sequential_current_state[1]_i_1_n_0\,
      Q => current_state(1)
    );
\FSM_sequential_current_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      CLR => AR(0),
      D => \FSM_sequential_current_state[2]_i_1_n_0\,
      Q => current_state(2)
    );
\LIGHT_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => current_state(1),
      I1 => current_state(0),
      I2 => current_state(2),
      O => LIGHT_OBUF(0)
    );
\LIGHT_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(2),
      I1 => current_state(0),
      I2 => current_state(1),
      O => LIGHT_OBUF(1)
    );
\LIGHT_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => current_state(1),
      I1 => current_state(2),
      I2 => current_state(0),
      O => LIGHT_OBUF(2)
    );
\LIGHT_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => current_state(1),
      I2 => current_state(2),
      O => LIGHT_OBUF(3)
    );
\LIGHT_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => current_state(2),
      I1 => current_state(1),
      I2 => current_state(0),
      O => LIGHT_OBUF(4)
    );
PRODUCT_OBUF_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => current_state(1),
      I1 => current_state(0),
      I2 => current_state(2),
      O => PRODUCT_OBUF
    );
\ticks[0]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(0),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_10_n_0\
    );
\ticks[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => ticks_reg(3),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_3_n_0\
    );
\ticks[0]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(2),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_4_n_0\
    );
\ticks[0]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(1),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_5_n_0\
    );
\ticks[0]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(0),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_6_n_0\
    );
\ticks[0]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => ticks_reg(3),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_7_n_0\
    );
\ticks[0]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(2),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_8_n_0\
    );
\ticks[0]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(1),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[0]_i_9_n_0\
    );
\ticks[12]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(15),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_2_n_0\
    );
\ticks[12]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(14),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_3_n_0\
    );
\ticks[12]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(13),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_4_n_0\
    );
\ticks[12]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(12),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_5_n_0\
    );
\ticks[12]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(15),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_6_n_0\
    );
\ticks[12]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(14),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_7_n_0\
    );
\ticks[12]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(13),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_8_n_0\
    );
\ticks[12]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(12),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[12]_i_9_n_0\
    );
\ticks[16]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(19),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_2_n_0\
    );
\ticks[16]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(18),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_3_n_0\
    );
\ticks[16]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(17),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_4_n_0\
    );
\ticks[16]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(16),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_5_n_0\
    );
\ticks[16]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(19),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_6_n_0\
    );
\ticks[16]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(18),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_7_n_0\
    );
\ticks[16]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(17),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_8_n_0\
    );
\ticks[16]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(16),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[16]_i_9_n_0\
    );
\ticks[20]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(23),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_2_n_0\
    );
\ticks[20]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(22),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_3_n_0\
    );
\ticks[20]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(21),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_4_n_0\
    );
\ticks[20]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(20),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_5_n_0\
    );
\ticks[20]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(23),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_6_n_0\
    );
\ticks[20]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(22),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_7_n_0\
    );
\ticks[20]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(21),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_8_n_0\
    );
\ticks[20]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(20),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[20]_i_9_n_0\
    );
\ticks[24]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(27),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_2_n_0\
    );
\ticks[24]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(26),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_3_n_0\
    );
\ticks[24]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(25),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_4_n_0\
    );
\ticks[24]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(24),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_5_n_0\
    );
\ticks[24]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(27),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_6_n_0\
    );
\ticks[24]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(26),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_7_n_0\
    );
\ticks[24]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(25),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_8_n_0\
    );
\ticks[24]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(24),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[24]_i_9_n_0\
    );
\ticks[28]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(30),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[28]_i_2_n_0\
    );
\ticks[28]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(29),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[28]_i_3_n_0\
    );
\ticks[28]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(28),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[28]_i_4_n_0\
    );
\ticks[28]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(31),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[28]_i_5_n_0\
    );
\ticks[28]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(30),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[28]_i_6_n_0\
    );
\ticks[28]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(29),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[28]_i_7_n_0\
    );
\ticks[28]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(28),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[28]_i_8_n_0\
    );
\ticks[4]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => ticks_reg(7),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_2_n_0\
    );
\ticks[4]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(6),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_3_n_0\
    );
\ticks[4]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => ticks_reg(5),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_4_n_0\
    );
\ticks[4]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => ticks_reg(4),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_5_n_0\
    );
\ticks[4]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => ticks_reg(7),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_6_n_0\
    );
\ticks[4]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(6),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_7_n_0\
    );
\ticks[4]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => ticks_reg(5),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_8_n_0\
    );
\ticks[4]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => ticks_reg(4),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[4]_i_9_n_0\
    );
\ticks[8]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => ticks_reg(11),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_2_n_0\
    );
\ticks[8]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => ticks_reg(10),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_3_n_0\
    );
\ticks[8]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => ticks_reg(9),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_4_n_0\
    );
\ticks[8]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => ticks_reg(8),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_5_n_0\
    );
\ticks[8]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => ticks_reg(11),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_6_n_0\
    );
\ticks[8]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => ticks_reg(10),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_7_n_0\
    );
\ticks[8]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => ticks_reg(9),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_8_n_0\
    );
\ticks[8]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => ticks_reg(8),
      I1 => current_state(2),
      I2 => current_state(0),
      O => \ticks[8]_i_9_n_0\
    );
\ticks_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ticks_reg[0]_i_2_n_0\,
      CO(2) => \ticks_reg[0]_i_2_n_1\,
      CO(1) => \ticks_reg[0]_i_2_n_2\,
      CO(0) => \ticks_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ticks[0]_i_3_n_0\,
      DI(2) => \ticks[0]_i_4_n_0\,
      DI(1) => \ticks[0]_i_5_n_0\,
      DI(0) => \ticks[0]_i_6_n_0\,
      O(3 downto 0) => O(3 downto 0),
      S(3) => \ticks[0]_i_7_n_0\,
      S(2) => \ticks[0]_i_8_n_0\,
      S(1) => \ticks[0]_i_9_n_0\,
      S(0) => \ticks[0]_i_10_n_0\
    );
\ticks_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ticks_reg[8]_i_1_n_0\,
      CO(3) => \ticks_reg[12]_i_1_n_0\,
      CO(2) => \ticks_reg[12]_i_1_n_1\,
      CO(1) => \ticks_reg[12]_i_1_n_2\,
      CO(0) => \ticks_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \ticks[12]_i_2_n_0\,
      DI(2) => \ticks[12]_i_3_n_0\,
      DI(1) => \ticks[12]_i_4_n_0\,
      DI(0) => \ticks[12]_i_5_n_0\,
      O(3 downto 0) => \ticks_reg[15]\(3 downto 0),
      S(3) => \ticks[12]_i_6_n_0\,
      S(2) => \ticks[12]_i_7_n_0\,
      S(1) => \ticks[12]_i_8_n_0\,
      S(0) => \ticks[12]_i_9_n_0\
    );
\ticks_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ticks_reg[12]_i_1_n_0\,
      CO(3) => \ticks_reg[16]_i_1_n_0\,
      CO(2) => \ticks_reg[16]_i_1_n_1\,
      CO(1) => \ticks_reg[16]_i_1_n_2\,
      CO(0) => \ticks_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \ticks[16]_i_2_n_0\,
      DI(2) => \ticks[16]_i_3_n_0\,
      DI(1) => \ticks[16]_i_4_n_0\,
      DI(0) => \ticks[16]_i_5_n_0\,
      O(3 downto 0) => \ticks_reg[19]\(3 downto 0),
      S(3) => \ticks[16]_i_6_n_0\,
      S(2) => \ticks[16]_i_7_n_0\,
      S(1) => \ticks[16]_i_8_n_0\,
      S(0) => \ticks[16]_i_9_n_0\
    );
\ticks_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ticks_reg[16]_i_1_n_0\,
      CO(3) => \ticks_reg[20]_i_1_n_0\,
      CO(2) => \ticks_reg[20]_i_1_n_1\,
      CO(1) => \ticks_reg[20]_i_1_n_2\,
      CO(0) => \ticks_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \ticks[20]_i_2_n_0\,
      DI(2) => \ticks[20]_i_3_n_0\,
      DI(1) => \ticks[20]_i_4_n_0\,
      DI(0) => \ticks[20]_i_5_n_0\,
      O(3 downto 0) => \ticks_reg[23]\(3 downto 0),
      S(3) => \ticks[20]_i_6_n_0\,
      S(2) => \ticks[20]_i_7_n_0\,
      S(1) => \ticks[20]_i_8_n_0\,
      S(0) => \ticks[20]_i_9_n_0\
    );
\ticks_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ticks_reg[20]_i_1_n_0\,
      CO(3) => \ticks_reg[24]_i_1_n_0\,
      CO(2) => \ticks_reg[24]_i_1_n_1\,
      CO(1) => \ticks_reg[24]_i_1_n_2\,
      CO(0) => \ticks_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \ticks[24]_i_2_n_0\,
      DI(2) => \ticks[24]_i_3_n_0\,
      DI(1) => \ticks[24]_i_4_n_0\,
      DI(0) => \ticks[24]_i_5_n_0\,
      O(3 downto 0) => \ticks_reg[27]\(3 downto 0),
      S(3) => \ticks[24]_i_6_n_0\,
      S(2) => \ticks[24]_i_7_n_0\,
      S(1) => \ticks[24]_i_8_n_0\,
      S(0) => \ticks[24]_i_9_n_0\
    );
\ticks_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ticks_reg[24]_i_1_n_0\,
      CO(3) => \NLW_ticks_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ticks_reg[28]_i_1_n_1\,
      CO(1) => \ticks_reg[28]_i_1_n_2\,
      CO(0) => \ticks_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \ticks[28]_i_2_n_0\,
      DI(1) => \ticks[28]_i_3_n_0\,
      DI(0) => \ticks[28]_i_4_n_0\,
      O(3 downto 0) => \ticks_reg[30]\(3 downto 0),
      S(3) => \ticks[28]_i_5_n_0\,
      S(2) => \ticks[28]_i_6_n_0\,
      S(1) => \ticks[28]_i_7_n_0\,
      S(0) => \ticks[28]_i_8_n_0\
    );
\ticks_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ticks_reg[0]_i_2_n_0\,
      CO(3) => \ticks_reg[4]_i_1_n_0\,
      CO(2) => \ticks_reg[4]_i_1_n_1\,
      CO(1) => \ticks_reg[4]_i_1_n_2\,
      CO(0) => \ticks_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \ticks[4]_i_2_n_0\,
      DI(2) => \ticks[4]_i_3_n_0\,
      DI(1) => \ticks[4]_i_4_n_0\,
      DI(0) => \ticks[4]_i_5_n_0\,
      O(3 downto 0) => \ticks_reg[7]\(3 downto 0),
      S(3) => \ticks[4]_i_6_n_0\,
      S(2) => \ticks[4]_i_7_n_0\,
      S(1) => \ticks[4]_i_8_n_0\,
      S(0) => \ticks[4]_i_9_n_0\
    );
\ticks_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ticks_reg[4]_i_1_n_0\,
      CO(3) => \ticks_reg[8]_i_1_n_0\,
      CO(2) => \ticks_reg[8]_i_1_n_1\,
      CO(1) => \ticks_reg[8]_i_1_n_2\,
      CO(0) => \ticks_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \ticks[8]_i_2_n_0\,
      DI(2) => \ticks[8]_i_3_n_0\,
      DI(1) => \ticks[8]_i_4_n_0\,
      DI(0) => \ticks[8]_i_5_n_0\,
      O(3 downto 0) => \ticks_reg[11]\(3 downto 0),
      S(3) => \ticks[8]_i_6_n_0\,
      S(2) => \ticks[8]_i_7_n_0\,
      S(1) => \ticks[8]_i_8_n_0\,
      S(0) => \ticks[8]_i_9_n_0\
    );
zero_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_state(0),
      I1 => current_state(2),
      O => \FSM_sequential_current_state_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity prescaler is
  port (
    CLK_P : out STD_LOGIC;
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end prescaler;

architecture STRUCTURE of prescaler is
  signal \^clk_p\ : STD_LOGIC;
  signal clk_out_i_i_1_n_0 : STD_LOGIC;
  signal cnt : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[15]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[15]_i_4_n_0\ : STD_LOGIC;
  signal \cnt[15]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[10]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[11]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[12]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[15]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[8]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[9]\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \plusOp_carry__0_n_0\ : STD_LOGIC;
  signal \plusOp_carry__0_n_1\ : STD_LOGIC;
  signal \plusOp_carry__0_n_2\ : STD_LOGIC;
  signal \plusOp_carry__0_n_3\ : STD_LOGIC;
  signal \plusOp_carry__1_n_0\ : STD_LOGIC;
  signal \plusOp_carry__1_n_1\ : STD_LOGIC;
  signal \plusOp_carry__1_n_2\ : STD_LOGIC;
  signal \plusOp_carry__1_n_3\ : STD_LOGIC;
  signal \plusOp_carry__2_n_2\ : STD_LOGIC;
  signal \plusOp_carry__2_n_3\ : STD_LOGIC;
  signal plusOp_carry_n_0 : STD_LOGIC;
  signal plusOp_carry_n_1 : STD_LOGIC;
  signal plusOp_carry_n_2 : STD_LOGIC;
  signal plusOp_carry_n_3 : STD_LOGIC;
  signal \NLW_plusOp_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_plusOp_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of plusOp_carry : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__2\ : label is 35;
begin
  CLK_P <= \^clk_p\;
clk_out_i_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => \^clk_p\,
      O => clk_out_i_i_1_n_0
    );
clk_out_i_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => clk_out_i_i_1_n_0,
      Q => \^clk_p\
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt[0]_i_2_n_0\,
      I1 => \cnt_reg_n_0_[0]\,
      O => cnt(0)
    );
\cnt[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFFF"
    )
        port map (
      I0 => \cnt[15]_i_5_n_0\,
      I1 => \cnt[15]_i_4_n_0\,
      I2 => \cnt_reg_n_0_[15]\,
      I3 => \cnt_reg_n_0_[14]\,
      I4 => \cnt_reg_n_0_[1]\,
      I5 => \cnt[15]_i_2_n_0\,
      O => \cnt[0]_i_2_n_0\
    );
\cnt[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(10),
      O => cnt(10)
    );
\cnt[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(11),
      O => cnt(11)
    );
\cnt[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(12),
      O => cnt(12)
    );
\cnt[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(13),
      O => cnt(13)
    );
\cnt[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(14),
      O => cnt(14)
    );
\cnt[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(15),
      O => cnt(15)
    );
\cnt[15]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_reg_n_0_[11]\,
      I1 => \cnt_reg_n_0_[10]\,
      I2 => \cnt_reg_n_0_[13]\,
      I3 => \cnt_reg_n_0_[12]\,
      O => \cnt[15]_i_2_n_0\
    );
\cnt[15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => \cnt_reg_n_0_[15]\,
      I1 => \cnt_reg_n_0_[14]\,
      I2 => \cnt_reg_n_0_[1]\,
      O => \cnt[15]_i_3_n_0\
    );
\cnt[15]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \cnt_reg_n_0_[3]\,
      I1 => \cnt_reg_n_0_[2]\,
      I2 => \cnt_reg_n_0_[4]\,
      I3 => \cnt_reg_n_0_[5]\,
      O => \cnt[15]_i_4_n_0\
    );
\cnt[15]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => \cnt_reg_n_0_[6]\,
      I1 => \cnt_reg_n_0_[7]\,
      I2 => \cnt_reg_n_0_[9]\,
      I3 => \cnt_reg_n_0_[8]\,
      O => \cnt[15]_i_5_n_0\
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(1),
      O => cnt(1)
    );
\cnt[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(2),
      O => cnt(2)
    );
\cnt[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(3),
      O => cnt(3)
    );
\cnt[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(4),
      O => cnt(4)
    );
\cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(5),
      O => cnt(5)
    );
\cnt[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(6),
      O => cnt(6)
    );
\cnt[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(7),
      O => cnt(7)
    );
\cnt[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(8),
      O => cnt(8)
    );
\cnt[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt[15]_i_2_n_0\,
      I1 => \cnt[15]_i_3_n_0\,
      I2 => \cnt[15]_i_4_n_0\,
      I3 => \cnt[15]_i_5_n_0\,
      I4 => \cnt_reg_n_0_[0]\,
      I5 => data0(9),
      O => cnt(9)
    );
\cnt_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(0),
      Q => \cnt_reg_n_0_[0]\
    );
\cnt_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(10),
      Q => \cnt_reg_n_0_[10]\
    );
\cnt_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(11),
      Q => \cnt_reg_n_0_[11]\
    );
\cnt_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(12),
      Q => \cnt_reg_n_0_[12]\
    );
\cnt_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(13),
      Q => \cnt_reg_n_0_[13]\
    );
\cnt_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(14),
      Q => \cnt_reg_n_0_[14]\
    );
\cnt_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(15),
      Q => \cnt_reg_n_0_[15]\
    );
\cnt_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(1),
      Q => \cnt_reg_n_0_[1]\
    );
\cnt_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(2),
      Q => \cnt_reg_n_0_[2]\
    );
\cnt_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(3),
      Q => \cnt_reg_n_0_[3]\
    );
\cnt_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(4),
      Q => \cnt_reg_n_0_[4]\
    );
\cnt_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(5),
      Q => \cnt_reg_n_0_[5]\
    );
\cnt_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(6),
      Q => \cnt_reg_n_0_[6]\
    );
\cnt_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(7),
      Q => \cnt_reg_n_0_[7]\
    );
\cnt_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(8),
      Q => \cnt_reg_n_0_[8]\
    );
\cnt_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => cnt(9),
      Q => \cnt_reg_n_0_[9]\
    );
plusOp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => plusOp_carry_n_0,
      CO(2) => plusOp_carry_n_1,
      CO(1) => plusOp_carry_n_2,
      CO(0) => plusOp_carry_n_3,
      CYINIT => \cnt_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \cnt_reg_n_0_[4]\,
      S(2) => \cnt_reg_n_0_[3]\,
      S(1) => \cnt_reg_n_0_[2]\,
      S(0) => \cnt_reg_n_0_[1]\
    );
\plusOp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => plusOp_carry_n_0,
      CO(3) => \plusOp_carry__0_n_0\,
      CO(2) => \plusOp_carry__0_n_1\,
      CO(1) => \plusOp_carry__0_n_2\,
      CO(0) => \plusOp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \cnt_reg_n_0_[8]\,
      S(2) => \cnt_reg_n_0_[7]\,
      S(1) => \cnt_reg_n_0_[6]\,
      S(0) => \cnt_reg_n_0_[5]\
    );
\plusOp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \plusOp_carry__0_n_0\,
      CO(3) => \plusOp_carry__1_n_0\,
      CO(2) => \plusOp_carry__1_n_1\,
      CO(1) => \plusOp_carry__1_n_2\,
      CO(0) => \plusOp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \cnt_reg_n_0_[12]\,
      S(2) => \cnt_reg_n_0_[11]\,
      S(1) => \cnt_reg_n_0_[10]\,
      S(0) => \cnt_reg_n_0_[9]\
    );
\plusOp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \plusOp_carry__1_n_0\,
      CO(3 downto 2) => \NLW_plusOp_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \plusOp_carry__2_n_2\,
      CO(0) => \plusOp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_plusOp_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(15 downto 13),
      S(3) => '0',
      S(2) => \cnt_reg_n_0_[15]\,
      S(1) => \cnt_reg_n_0_[14]\,
      S(0) => \cnt_reg_n_0_[13]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity timer is
  port (
    ZERO : out STD_LOGIC;
    ticks_reg : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_P_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[15]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[19]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[23]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[27]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ticks_reg[31]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    zero_reg_0 : in STD_LOGIC
  );
end timer;

architecture STRUCTURE of timer is
  signal \ticks[0]_i_1_n_0\ : STD_LOGIC;
  signal \^ticks_reg\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal zero_i_10_n_0 : STD_LOGIC;
  signal zero_i_1_n_0 : STD_LOGIC;
  signal zero_i_2_n_0 : STD_LOGIC;
  signal zero_i_3_n_0 : STD_LOGIC;
  signal zero_i_4_n_0 : STD_LOGIC;
  signal zero_i_5_n_0 : STD_LOGIC;
  signal zero_i_7_n_0 : STD_LOGIC;
  signal zero_i_8_n_0 : STD_LOGIC;
  signal zero_i_9_n_0 : STD_LOGIC;
begin
  ticks_reg(31 downto 0) <= \^ticks_reg\(31 downto 0);
\ticks[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => zero_i_5_n_0,
      I1 => zero_i_4_n_0,
      I2 => zero_i_3_n_0,
      I3 => zero_i_2_n_0,
      O => \ticks[0]_i_1_n_0\
    );
\ticks_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => O(0),
      Q => \^ticks_reg\(0)
    );
\ticks_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[11]_0\(2),
      Q => \^ticks_reg\(10)
    );
\ticks_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[11]_0\(3),
      Q => \^ticks_reg\(11)
    );
\ticks_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[15]_0\(0),
      Q => \^ticks_reg\(12)
    );
\ticks_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[15]_0\(1),
      Q => \^ticks_reg\(13)
    );
\ticks_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[15]_0\(2),
      Q => \^ticks_reg\(14)
    );
\ticks_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[15]_0\(3),
      Q => \^ticks_reg\(15)
    );
\ticks_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[19]_0\(0),
      Q => \^ticks_reg\(16)
    );
\ticks_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[19]_0\(1),
      Q => \^ticks_reg\(17)
    );
\ticks_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[19]_0\(2),
      Q => \^ticks_reg\(18)
    );
\ticks_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[19]_0\(3),
      Q => \^ticks_reg\(19)
    );
\ticks_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => O(1),
      Q => \^ticks_reg\(1)
    );
\ticks_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[23]_0\(0),
      Q => \^ticks_reg\(20)
    );
\ticks_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[23]_0\(1),
      Q => \^ticks_reg\(21)
    );
\ticks_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[23]_0\(2),
      Q => \^ticks_reg\(22)
    );
\ticks_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[23]_0\(3),
      Q => \^ticks_reg\(23)
    );
\ticks_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[27]_0\(0),
      Q => \^ticks_reg\(24)
    );
\ticks_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[27]_0\(1),
      Q => \^ticks_reg\(25)
    );
\ticks_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[27]_0\(2),
      Q => \^ticks_reg\(26)
    );
\ticks_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[27]_0\(3),
      Q => \^ticks_reg\(27)
    );
\ticks_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[31]_0\(0),
      Q => \^ticks_reg\(28)
    );
\ticks_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[31]_0\(1),
      Q => \^ticks_reg\(29)
    );
\ticks_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => O(2),
      Q => \^ticks_reg\(2)
    );
\ticks_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[31]_0\(2),
      Q => \^ticks_reg\(30)
    );
\ticks_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[31]_0\(3),
      Q => \^ticks_reg\(31)
    );
\ticks_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => O(3),
      Q => \^ticks_reg\(3)
    );
\ticks_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[7]_0\(0),
      Q => \^ticks_reg\(4)
    );
\ticks_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[7]_0\(1),
      Q => \^ticks_reg\(5)
    );
\ticks_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[7]_0\(2),
      Q => \^ticks_reg\(6)
    );
\ticks_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[7]_0\(3),
      Q => \^ticks_reg\(7)
    );
\ticks_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[11]_0\(0),
      Q => \^ticks_reg\(8)
    );
\ticks_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => \ticks[0]_i_1_n_0\,
      CLR => AR(0),
      D => \ticks_reg[11]_0\(1),
      Q => \^ticks_reg\(9)
    );
zero_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => zero_i_2_n_0,
      I1 => zero_i_3_n_0,
      I2 => zero_i_4_n_0,
      I3 => zero_i_5_n_0,
      O => zero_i_1_n_0
    );
zero_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^ticks_reg\(25),
      I1 => \^ticks_reg\(29),
      I2 => \^ticks_reg\(12),
      I3 => \^ticks_reg\(21),
      O => zero_i_10_n_0
    );
zero_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => zero_reg_0,
      I1 => \^ticks_reg\(4),
      I2 => zero_i_7_n_0,
      I3 => \^ticks_reg\(8),
      I4 => \^ticks_reg\(23),
      I5 => \^ticks_reg\(27),
      O => zero_i_2_n_0
    );
zero_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^ticks_reg\(28),
      I1 => \^ticks_reg\(26),
      I2 => \^ticks_reg\(3),
      I3 => \^ticks_reg\(2),
      I4 => zero_i_8_n_0,
      O => zero_i_3_n_0
    );
zero_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^ticks_reg\(17),
      I1 => \^ticks_reg\(6),
      I2 => \^ticks_reg\(31),
      I3 => \^ticks_reg\(7),
      I4 => zero_i_9_n_0,
      O => zero_i_4_n_0
    );
zero_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^ticks_reg\(13),
      I1 => \^ticks_reg\(5),
      I2 => \^ticks_reg\(30),
      I3 => \^ticks_reg\(14),
      I4 => zero_i_10_n_0,
      O => zero_i_5_n_0
    );
zero_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^ticks_reg\(9),
      I1 => \^ticks_reg\(11),
      I2 => \^ticks_reg\(10),
      I3 => \^ticks_reg\(22),
      O => zero_i_7_n_0
    );
zero_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^ticks_reg\(18),
      I1 => \^ticks_reg\(19),
      I2 => \^ticks_reg\(1),
      I3 => \^ticks_reg\(15),
      O => zero_i_8_n_0
    );
zero_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^ticks_reg\(16),
      I1 => \^ticks_reg\(20),
      I2 => \^ticks_reg\(0),
      I3 => \^ticks_reg\(24),
      O => zero_i_9_n_0
    );
zero_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_P_BUFG,
      CE => '1',
      CLR => AR(0),
      D => zero_i_1_n_0,
      Q => ZERO
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    RESET : in STD_LOGIC;
    CLK : in STD_LOGIC;
    COINS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PRODUCT : out STD_LOGIC;
    ERROR : out STD_LOGIC;
    LIGHT : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of top : entity is true;
  attribute CLKIN_FREQ : integer;
  attribute CLKIN_FREQ of top : entity is 100000000;
  attribute CLKOUT_FREQ : integer;
  attribute CLKOUT_FREQ of top : entity is 1000;
  attribute DLY : integer;
  attribute DLY of top : entity is 3;
end top;

architecture STRUCTURE of top is
  signal CLK_IBUF : STD_LOGIC;
  signal CLK_IBUF_BUFG : STD_LOGIC;
  signal CLK_P : STD_LOGIC;
  signal CLK_P_BUFG : STD_LOGIC;
  signal COINS_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ERROR_OBUF : STD_LOGIC;
  signal LIGHT_OBUF : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal PRODUCT_OBUF : STD_LOGIC;
  signal Q1 : STD_LOGIC;
  signal Q1_1 : STD_LOGIC;
  signal Q1_4 : STD_LOGIC;
  signal Q2 : STD_LOGIC;
  signal Q2_2 : STD_LOGIC;
  signal Q2_5 : STD_LOGIC;
  signal Q3 : STD_LOGIC;
  signal Q3_0 : STD_LOGIC;
  signal Q3_3 : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal ZERO : STD_LOGIC;
  signal accum_i : STD_LOGIC;
  signal accum_i_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal inst_coin_counter_n_1 : STD_LOGIC;
  signal inst_coin_counter_n_2 : STD_LOGIC;
  signal inst_fsm_n_10 : STD_LOGIC;
  signal inst_fsm_n_11 : STD_LOGIC;
  signal inst_fsm_n_12 : STD_LOGIC;
  signal inst_fsm_n_13 : STD_LOGIC;
  signal inst_fsm_n_14 : STD_LOGIC;
  signal inst_fsm_n_15 : STD_LOGIC;
  signal inst_fsm_n_16 : STD_LOGIC;
  signal inst_fsm_n_17 : STD_LOGIC;
  signal inst_fsm_n_18 : STD_LOGIC;
  signal inst_fsm_n_19 : STD_LOGIC;
  signal inst_fsm_n_20 : STD_LOGIC;
  signal inst_fsm_n_21 : STD_LOGIC;
  signal inst_fsm_n_22 : STD_LOGIC;
  signal inst_fsm_n_23 : STD_LOGIC;
  signal inst_fsm_n_24 : STD_LOGIC;
  signal inst_fsm_n_25 : STD_LOGIC;
  signal inst_fsm_n_26 : STD_LOGIC;
  signal inst_fsm_n_27 : STD_LOGIC;
  signal inst_fsm_n_28 : STD_LOGIC;
  signal inst_fsm_n_29 : STD_LOGIC;
  signal inst_fsm_n_30 : STD_LOGIC;
  signal inst_fsm_n_31 : STD_LOGIC;
  signal inst_fsm_n_32 : STD_LOGIC;
  signal inst_fsm_n_33 : STD_LOGIC;
  signal inst_fsm_n_34 : STD_LOGIC;
  signal inst_fsm_n_35 : STD_LOGIC;
  signal inst_fsm_n_36 : STD_LOGIC;
  signal inst_fsm_n_37 : STD_LOGIC;
  signal inst_fsm_n_38 : STD_LOGIC;
  signal inst_fsm_n_39 : STD_LOGIC;
  signal inst_fsm_n_7 : STD_LOGIC;
  signal inst_fsm_n_8 : STD_LOGIC;
  signal inst_fsm_n_9 : STD_LOGIC;
  signal \synchronizers[0].inst_debouncer_i_n_3\ : STD_LOGIC;
  signal \synchronizers[0].inst_synchronizer_i_n_0\ : STD_LOGIC;
  signal \synchronizers[1].inst_debouncer_i_n_3\ : STD_LOGIC;
  signal \synchronizers[1].inst_debouncer_i_n_4\ : STD_LOGIC;
  signal \synchronizers[1].inst_synchronizer_i_n_0\ : STD_LOGIC;
  signal \synchronizers[2].inst_debouncer_i_n_3\ : STD_LOGIC;
  signal \synchronizers[2].inst_synchronizer_i_n_0\ : STD_LOGIC;
  signal \synchronizers[3].inst_synchronizer_i_n_0\ : STD_LOGIC;
  signal ticks_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
CLK_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_IBUF,
      O => CLK_IBUF_BUFG
    );
CLK_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => CLK,
      O => CLK_IBUF
    );
CLK_P_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_P,
      O => CLK_P_BUFG
    );
\COINS_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => COINS(0),
      O => COINS_IBUF(0)
    );
\COINS_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => COINS(1),
      O => COINS_IBUF(1)
    );
\COINS_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => COINS(2),
      O => COINS_IBUF(2)
    );
\COINS_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => COINS(3),
      O => COINS_IBUF(3)
    );
ERROR_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => ERROR_OBUF,
      O => ERROR
    );
\LIGHT_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LIGHT_OBUF(0),
      O => LIGHT(0)
    );
\LIGHT_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LIGHT_OBUF(1),
      O => LIGHT(1)
    );
\LIGHT_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LIGHT_OBUF(2),
      O => LIGHT(2)
    );
\LIGHT_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LIGHT_OBUF(3),
      O => LIGHT(3)
    );
\LIGHT_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LIGHT_OBUF(4),
      O => LIGHT(4)
    );
PRODUCT_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => PRODUCT_OBUF,
      O => PRODUCT
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
inst_coin_counter: entity work.count
     port map (
      AR(0) => RESET_IBUF,
      CLK_P_BUFG => CLK_P_BUFG,
      E(0) => accum_i,
      Q1 => Q1_4,
      Q1_0 => Q1,
      Q1_5 => Q1_1,
      Q2 => Q2_5,
      Q2_1 => Q2,
      Q2_4 => Q2_2,
      Q3 => Q3_3,
      Q3_2 => Q3,
      Q3_3 => Q3_0,
      S(0) => \synchronizers[2].inst_debouncer_i_n_3\,
      \accum_i_reg[3]_0\ => \synchronizers[1].inst_debouncer_i_n_4\,
      \accum_i_reg[3]_1\ => \synchronizers[0].inst_debouncer_i_n_3\,
      \accum_i_reg[6]_0\ => inst_coin_counter_n_2,
      \accum_i_reg[7]_0\ => inst_coin_counter_n_1,
      \accum_i_reg[7]_1\ => \synchronizers[1].inst_debouncer_i_n_3\,
      \out\(0) => accum_i_reg(1)
    );
inst_fsm: entity work.fsm
     port map (
      AR(0) => RESET_IBUF,
      CLK_P_BUFG => CLK_P_BUFG,
      ERROR_OBUF => ERROR_OBUF,
      \FSM_sequential_current_state_reg[0]_0\ => inst_fsm_n_7,
      \FSM_sequential_current_state_reg[0]_1\ => inst_coin_counter_n_2,
      \FSM_sequential_current_state_reg[1]_0\ => inst_coin_counter_n_1,
      LIGHT_OBUF(4 downto 0) => LIGHT_OBUF(4 downto 0),
      O(3) => inst_fsm_n_8,
      O(2) => inst_fsm_n_9,
      O(1) => inst_fsm_n_10,
      O(0) => inst_fsm_n_11,
      PRODUCT_OBUF => PRODUCT_OBUF,
      ZERO => ZERO,
      ticks_reg(31 downto 0) => ticks_reg(31 downto 0),
      \ticks_reg[11]\(3) => inst_fsm_n_16,
      \ticks_reg[11]\(2) => inst_fsm_n_17,
      \ticks_reg[11]\(1) => inst_fsm_n_18,
      \ticks_reg[11]\(0) => inst_fsm_n_19,
      \ticks_reg[15]\(3) => inst_fsm_n_20,
      \ticks_reg[15]\(2) => inst_fsm_n_21,
      \ticks_reg[15]\(1) => inst_fsm_n_22,
      \ticks_reg[15]\(0) => inst_fsm_n_23,
      \ticks_reg[19]\(3) => inst_fsm_n_24,
      \ticks_reg[19]\(2) => inst_fsm_n_25,
      \ticks_reg[19]\(1) => inst_fsm_n_26,
      \ticks_reg[19]\(0) => inst_fsm_n_27,
      \ticks_reg[23]\(3) => inst_fsm_n_28,
      \ticks_reg[23]\(2) => inst_fsm_n_29,
      \ticks_reg[23]\(1) => inst_fsm_n_30,
      \ticks_reg[23]\(0) => inst_fsm_n_31,
      \ticks_reg[27]\(3) => inst_fsm_n_32,
      \ticks_reg[27]\(2) => inst_fsm_n_33,
      \ticks_reg[27]\(1) => inst_fsm_n_34,
      \ticks_reg[27]\(0) => inst_fsm_n_35,
      \ticks_reg[30]\(3) => inst_fsm_n_36,
      \ticks_reg[30]\(2) => inst_fsm_n_37,
      \ticks_reg[30]\(1) => inst_fsm_n_38,
      \ticks_reg[30]\(0) => inst_fsm_n_39,
      \ticks_reg[7]\(3) => inst_fsm_n_12,
      \ticks_reg[7]\(2) => inst_fsm_n_13,
      \ticks_reg[7]\(1) => inst_fsm_n_14,
      \ticks_reg[7]\(0) => inst_fsm_n_15
    );
inst_prescaler: entity work.prescaler
     port map (
      AR(0) => RESET_IBUF,
      CLK => CLK_IBUF_BUFG,
      CLK_P => CLK_P
    );
inst_timer: entity work.timer
     port map (
      AR(0) => RESET_IBUF,
      CLK_P_BUFG => CLK_P_BUFG,
      O(3) => inst_fsm_n_8,
      O(2) => inst_fsm_n_9,
      O(1) => inst_fsm_n_10,
      O(0) => inst_fsm_n_11,
      ZERO => ZERO,
      ticks_reg(31 downto 0) => ticks_reg(31 downto 0),
      \ticks_reg[11]_0\(3) => inst_fsm_n_16,
      \ticks_reg[11]_0\(2) => inst_fsm_n_17,
      \ticks_reg[11]_0\(1) => inst_fsm_n_18,
      \ticks_reg[11]_0\(0) => inst_fsm_n_19,
      \ticks_reg[15]_0\(3) => inst_fsm_n_20,
      \ticks_reg[15]_0\(2) => inst_fsm_n_21,
      \ticks_reg[15]_0\(1) => inst_fsm_n_22,
      \ticks_reg[15]_0\(0) => inst_fsm_n_23,
      \ticks_reg[19]_0\(3) => inst_fsm_n_24,
      \ticks_reg[19]_0\(2) => inst_fsm_n_25,
      \ticks_reg[19]_0\(1) => inst_fsm_n_26,
      \ticks_reg[19]_0\(0) => inst_fsm_n_27,
      \ticks_reg[23]_0\(3) => inst_fsm_n_28,
      \ticks_reg[23]_0\(2) => inst_fsm_n_29,
      \ticks_reg[23]_0\(1) => inst_fsm_n_30,
      \ticks_reg[23]_0\(0) => inst_fsm_n_31,
      \ticks_reg[27]_0\(3) => inst_fsm_n_32,
      \ticks_reg[27]_0\(2) => inst_fsm_n_33,
      \ticks_reg[27]_0\(1) => inst_fsm_n_34,
      \ticks_reg[27]_0\(0) => inst_fsm_n_35,
      \ticks_reg[31]_0\(3) => inst_fsm_n_36,
      \ticks_reg[31]_0\(2) => inst_fsm_n_37,
      \ticks_reg[31]_0\(1) => inst_fsm_n_38,
      \ticks_reg[31]_0\(0) => inst_fsm_n_39,
      \ticks_reg[7]_0\(3) => inst_fsm_n_12,
      \ticks_reg[7]_0\(2) => inst_fsm_n_13,
      \ticks_reg[7]_0\(1) => inst_fsm_n_14,
      \ticks_reg[7]_0\(0) => inst_fsm_n_15,
      zero_reg_0 => inst_fsm_n_7
    );
\synchronizers[0].inst_debouncer_i\: entity work.debouncer
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      Q1 => Q1,
      Q1_reg_0 => \synchronizers[0].inst_synchronizer_i_n_0\,
      Q2 => Q2,
      Q3 => Q3,
      Q3_reg_0 => \synchronizers[0].inst_debouncer_i_n_3\
    );
\synchronizers[0].inst_synchronizer_i\: entity work.SYNCHRNZR
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      COINS_IBUF(0) => COINS_IBUF(0),
      \sreg_reg[0]_0\ => \synchronizers[0].inst_synchronizer_i_n_0\
    );
\synchronizers[1].inst_debouncer_i\: entity work.debouncer_0
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      Q1 => Q1_1,
      Q1_0 => Q1_4,
      Q1_reg_0 => \synchronizers[1].inst_debouncer_i_n_3\,
      Q1_reg_1 => \synchronizers[1].inst_synchronizer_i_n_0\,
      Q2 => Q2_2,
      Q2_1 => Q2_5,
      Q3 => Q3_0,
      Q3_2 => Q3_3,
      Q3_reg_0 => \synchronizers[1].inst_debouncer_i_n_4\
    );
\synchronizers[1].inst_synchronizer_i\: entity work.SYNCHRNZR_1
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      COINS_IBUF(0) => COINS_IBUF(1),
      \sreg_reg[0]_0\ => \synchronizers[1].inst_synchronizer_i_n_0\
    );
\synchronizers[2].inst_debouncer_i\: entity work.debouncer_2
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      Q1 => Q1_4,
      Q1_reg_0 => \synchronizers[2].inst_synchronizer_i_n_0\,
      Q2 => Q2_5,
      Q3 => Q3_3,
      S(0) => \synchronizers[2].inst_debouncer_i_n_3\,
      \accum_i_reg[3]\ => \synchronizers[1].inst_debouncer_i_n_4\,
      \accum_i_reg[3]_0\ => \synchronizers[0].inst_debouncer_i_n_3\,
      \out\(0) => accum_i_reg(1)
    );
\synchronizers[2].inst_synchronizer_i\: entity work.SYNCHRNZR_3
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      COINS_IBUF(0) => COINS_IBUF(2),
      \sreg_reg[0]_0\ => \synchronizers[2].inst_synchronizer_i_n_0\
    );
\synchronizers[3].inst_debouncer_i\: entity work.debouncer_4
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      E(0) => accum_i,
      Q1_reg_0 => \synchronizers[3].inst_synchronizer_i_n_0\,
      \accum_i_reg[7]\ => \synchronizers[1].inst_debouncer_i_n_3\,
      \accum_i_reg[7]_0\ => \synchronizers[0].inst_debouncer_i_n_3\
    );
\synchronizers[3].inst_synchronizer_i\: entity work.SYNCHRNZR_5
     port map (
      CLK_P_BUFG => CLK_P_BUFG,
      COINS_IBUF(0) => COINS_IBUF(3),
      \sreg_reg[0]_0\ => \synchronizers[3].inst_synchronizer_i_n_0\
    );
end STRUCTURE;
